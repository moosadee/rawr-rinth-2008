#include <iostream>
#include <math.h>
#include "allegro.h"
#include "Character.h"
#include "Game.h"
#include "Level.h"
#include "Item.h"
#include "TextEffect.h"
#include "Enemy.h"
using namespace std;

void UpdateOffset( int *offsetX, int *offsetY, Character *player, Game *game );
bool IsCollision( Character *player, Level *level, int dir );
volatile long counter = 0;
void IncrementCounter();
bool IsCollision( Rect r1, Rect r2 );
bool IsCollision( Character *player, Level *level, int dir );
bool IsCollision( Enemy *player, Level *level, int dir );
bool IsCollision( Character *player, Item *item );
bool IsCollision( Character *player, Enemy *enemy );
float Distance( int x1, int y1, int x2, int y2 );
void DrawMenuText( BITMAP *buffer );
void TextAnimation( BITMAP *buffer, string text, float counter );
void DrawMiniMap( BITMAP *buffer, Level *level, Character *player, Character *player2 );

/*
RAWR RINTH
v 0.4 - game prototype
By Rachel J. Morris
http://www.moosader.com

Messy code, yay!
*/

const int MaxText = 20;     //Maximum amount of text on screen at once (too lazy to use dynamic array!)

const int VIEWDIST = 250;   //distance enemies can see


int main()
{
    Game game;

    //Timer stuff handles FPS regulation
    LOCK_VARIABLE( counter );
    LOCK_FUNCTION( IncrementCounter );
    install_int_ex( IncrementCounter,BPS_TO_TIMER(90) );
    install_mouse();

    //These probably should go somewhere else.  Oh well.
    int fruitCollected = 0, enemiesKilled = 0;
    int offsetX = 0, offsetY = 0, frame = 0;
    float gameTimer = 0.0;
    float soundTimer = 0, soundTimer2 = 0;
    bool drawMiniMap = true;

    //Make sure there's a pause between key repeats for toggles and such.
    float keyTimer = 0;

    TextEffect txteffect[MaxText];

    Level level[1];
    level[0].LoadMap( "content/lv-00.map" );
    //Sounds
    SAMPLE *sndPickup = load_sample( "content/ding.wav" );
    SAMPLE *sndAttack = load_sample( "content/attack.wav" );
    SAMPLE *sndDamage = load_sample( "content/explosion.wav" );
    SAMPLE *sndConfirm = load_sample( "content/sfx2.wav" );
    SAMPLE *sndDeath = load_sample( "content/death.wav" );
    SAMPLE *sndLevelUp = load_sample( "content/levelup.wav" );
    SAMPLE *sndBack = load_sample( "content/sfx.wav" );
    SAMPLE *song1 = load_sample( "content/Dinosaur.wav" );
    //Graphics
    BITMAP *imgTileset = load_bitmap( "content/tileset2.bmp", NULL );
    BITMAP *imgPlayer = load_bitmap( "content/rawr.bmp", NULL );
    BITMAP *imgPlayer2 = load_bitmap( "content/rawr2.bmp", NULL );
    BITMAP *imgRobot = load_bitmap( "content/robot.bmp", NULL );
    BITMAP *imgEwok = load_bitmap( "content/ewok.bmp", NULL );
    BITMAP *imgSnake = load_bitmap( "content/snake.bmp", NULL );
    BITMAP *imgKitty = load_bitmap( "content/kitty.bmp", NULL );
    BITMAP *imgBackground = load_bitmap( "content/bg.bmp", NULL );
    BITMAP *imgCherry = load_bitmap( "content/cherry.bmp", NULL );
    BITMAP *imgIcecream = load_bitmap( "content/icecream.bmp", NULL );
    BITMAP *imgEgg = load_bitmap( "content/egg.bmp", NULL );
    BITMAP *imgMoney = load_bitmap( "content/coins.bmp", NULL );
    BITMAP *imgHudItem = load_bitmap( "content/items.bmp", NULL );
    BITMAP *imgTitleScreen = load_bitmap( "content/title.bmp", NULL );
    BITMAP *imgGameOver = load_bitmap( "content/gameover.bmp", NULL );
    BITMAP *buffer = create_bitmap( game.ScreenWidth(), game.ScreenHeight() );

    if(imgPlayer2 == NULL) { allegro_message("bitmap loading error"); allegro_exit(); }

    Enemy enemy[ level[0].EnemyCount() ];
    for ( int i=0; i<level[0].EnemyCount() ; i++ )      //Init enemies based on index
    {
        enemy[i].Setup( i );
    }
    Item item[ level[0].ItemCount() ];
    for ( int i=0; i<level[0].ItemCount(); i++ )        //Init items based on index
    {
        item[i].Setup( i );
    }

    Character player[2];
    player[0].Setup( 0 );
    player[1].Setup( 1 );
//    player[1].Exists( false );

//    enable_hardware_cursor();
//    select_mouse_cursor( MOUSE_CURSOR_ARROW );
//    show_mouse( buffer );

    game.State( sMENU );        //Game begins in menu
    while ( game.Done() == false )
    {
        while ( counter > 0 )   //Regulate FPS
        {

        poll_joystick();


        if ( key[KEY_F4] ) { game.Done( true ); }       //Quit button
            //Menu
            if ( game.State() == sMENU )
            {
                if ( key[KEY_ENTER] || joy[0].button[0].b )
                {
                    play_sample( sndConfirm, 255, 128, 1000, false );
                    game.State( sINGAME );
                    stop_sample( song1 );
                    play_sample( song1, 200, 128, 1000, true );
                }

            } // end menu
            if ( key[KEY_F5] )
            {
                game.ToggleFullscreen();
            }
            //Game
            else if ( game.State() == sINGAME )
            {
                if ( key[KEY_ESC] )
                {
                    play_sample( sndBack, 255, 128, 1000, false );
                    game.State( sMENU );
                    stop_sample( song1 );
                }

                bool isMoving = false;

                //PLAYER MOVEMENT
                if ( key[KEY_UP] || joy[0].stick[0].axis[1].d1 == 1 )
                {
                    if ( ( key[KEY_RSHIFT] || joy[0].button[1].b) && player[0].Stamina() > 0 ) { player[0].Speed( player[0].RUN() ); player[0].DrainStamina(); }
                    else { player[0].Speed( player[0].WALK() ); player[0].RestoreStamina(); }
                    if ( IsCollision( &player[0], &level[0], UP) == false ) { player[0].Move( UP ); }
                    isMoving = true;
                }
                else if ( key[KEY_DOWN] || joy[0].stick[0].axis[1].d2 == 1 )
                {
                    if ( ( key[KEY_RSHIFT] || joy[0].button[1].b) && player[0].Stamina() > 0 ) { player[0].Speed( player[0].RUN() ); player[0].DrainStamina(); }
                    else { player[0].Speed( player[0].WALK() ); player[0].RestoreStamina(); }
                    if ( IsCollision( &player[0], &level[0], DOWN) == false ) { player[0].Move( DOWN ); }
                    isMoving = true;
                }
                if ( key[KEY_LEFT] || joy[0].stick[0].axis[0].d1 == 1 )
                {
                    if ( ( key[KEY_RSHIFT] || joy[0].button[1].b) && player[0].Stamina() > 0 ) { player[0].Speed( player[0].RUN() ); player[0].DrainStamina(); }
                    else { player[0].Speed( player[0].WALK() ); player[0].RestoreStamina(); }
                    if ( IsCollision( &player[0], &level[0], LEFT ) == false ) { player[0].Move( LEFT ); }
                    isMoving = true;
                }
                else if ( key[KEY_RIGHT] || joy[0].stick[0].axis[0].d2 == 1 )
                {
                    if ( ( key[KEY_RSHIFT] || joy[0].button[1].b) && player[0].Stamina() > 0 ) { player[0].Speed( player[0].RUN() ); player[0].DrainStamina(); }
                    else { player[0].Speed( player[0].WALK() ); player[0].RestoreStamina(); }
                    if ( IsCollision( &player[0], &level[0], RIGHT) == false ) { player[0].Move( RIGHT ); }
                    isMoving = true;
                }

//                cout << player[0].X() << ", " << player[0].Y() << endl
//                    << player[1].X() << ", " << player[1].Y() << endl << endl;

                //PLAYER MOVEMENT
                // Up
//                if ( key[KEY_W] || joy[1].stick[0].axis[1].d1 == 1 )
                if ( mouse_y < player[1].Y() + 16 - offsetY )
                {
                    if ( ( key[KEY_LSHIFT] || joy[1].button[1].b) && player[1].Stamina() > 0 ) { player[1].Speed( player[1].RUN() ); player[1].DrainStamina(); }
                    else { player[1].Speed( player[1].WALK() ); player[1].RestoreStamina(); }

                    if ( IsCollision( &player[1], &level[0], UP) == false ) { player[1].Move( UP, player[0].X(), player[0].Y() ); }
                    isMoving = true;
                }
                // Down
//                else if ( key[KEY_S] || joy[1].stick[0].axis[1].d2 == 1 )
                if ( mouse_y > player[1].Y() + 16 - offsetY )
                {
                    if ( (key[KEY_LSHIFT] || joy[1].button[1].b) && player[1].Stamina() > 0 ) { player[1].Speed( player[1].RUN() ); player[1].DrainStamina(); }
                    else { player[1].Speed( player[1].WALK() ); player[1].RestoreStamina(); }
                    if ( IsCollision( &player[1], &level[0], DOWN) == false ) { player[1].Move( DOWN, player[0].X(), player[0].Y() ); }
                    isMoving = true;
                }
                // Left
//                if ( key[KEY_A] || joy[1].stick[0].axis[0].d1 == 1 )
                if ( mouse_x < player[1].X() + 16 - offsetX )
                {
                    if ( (key[KEY_LSHIFT] || joy[1].button[1].b) && player[1].Stamina() > 0 ) { player[1].Speed( player[1].RUN() ); player[1].DrainStamina(); }
                    else { player[1].Speed( player[1].WALK() ); player[1].RestoreStamina(); }
                    if ( IsCollision( &player[1], &level[0], LEFT ) == false ) { player[1].Move( LEFT, player[0].X(), player[0].Y() ); }
                    isMoving = true;
                }
                // Right
//                else if ( key[KEY_D] || joy[1].stick[0].axis[0].d2 == 1 )
                if ( mouse_x > player[1].X() + 16 - offsetX )
                {
                    if ( ( key[KEY_LSHIFT] || joy[1].button[1].b) && player[1].Stamina() > 0 ) { player[1].Speed( player[1].RUN() ); player[1].DrainStamina(); }
                    else { player[1].Speed( player[1].WALK() ); player[1].RestoreStamina(); }
                    if ( IsCollision( &player[1], &level[0], RIGHT) == false ) { player[1].Move( RIGHT, player[0].X(), player[0].Y() ); }
                    isMoving = true;
                }

                if ( key[KEY_RCONTROL] || joy[0].button[0].b )
                {
                    //player attacks
                    player[0].BeginAttack();
                }

                if ( key[KEY_SPACE] || joy[1].button[0].b || ( mouse_b & 1 ) )
                {
                    //player attacks
                    player[1].BeginAttack();
                }

                if ( key[KEY_F5] )
                {
                    game.ToggleFullscreen();
                }

                //Update enemies
                for ( int i=0; i<level[0].EnemyCount() ; i++ )
                {
                    for ( int p = 0; p < 2; p++ )
                    {
                        if ( Distance( enemy[i].X(), enemy[i].Y(), player[p].X(), player[p].Y() ) <= VIEWDIST )
                        {
                            //chase player
                            if ( (int)gameTimer % 3 == 0 )
                            {
                                if ( player[p].Y() < enemy[i].Y() && IsCollision( &enemy[i], &level[0], UP ) == false )
                                {
                                    enemy[i].Move( UP );
                                }
                                else if ( player[p].Y() > enemy[i].Y() && IsCollision( &enemy[i], &level[0], DOWN ) == false )
                                {
                                    enemy[i].Move( DOWN );
                                }
                            }
                            else
                            {
                                if ( player[p].X() < enemy[i].X() && IsCollision( &enemy[i], &level[0], LEFT ) == false )
                                {
                                    enemy[i].Move( LEFT );
                                }
                                else if ( player[p].X() > enemy[i].X() && IsCollision( &enemy[i], &level[0], RIGHT ) == false )
                                {
                                    enemy[i].Move( RIGHT );
                                }
                            }
                        }
                    }
                }

                player[0].Update( player[1].X(), player[1].Y() );
                player[1].Update( player[0].X(), player[0].Y() );

                for ( int i=0; i<MaxText; i++ )       //update text animation
                {
                    if ( txteffect[i].inUse )
                        txteffect[i].Update();
                }

                //Check collisions
                for ( int i=0; i<level[0].ItemCount(); i++ )
                {
                    if ( item[i].Exists() == false )
                    {
                        item[i].deadTimer -= 0.1;
                        cout << "Item " << item[i].deadTimer << endl;
                        if ( item[i].deadTimer <= 0 )
                        {
                            item[i].Exists( true );
                            item[i].Reset();
                            item[i].Setup( item[i].index );
                            cout << "RESPAWN ITEM" << endl;
                        }
                    }

                    for ( int p = 0; p < 2; p++ )
                    {
                        if ( item[i].Exists() == true && IsCollision( &player[p], &item[i] ) )
                        {
                            item[i].deadTimer = 100;
                            item[i].Exists( false );
                            player[p].AddScore( item[i].AdjustScore() );
                            player[p].AddHP( item[i].AdjustHP() );
                            player[p].AddMonies( item[i].AdjustMoney() );
                            play_sample( sndPickup, 255, 128, 1000, false );
                            if ( item[i].Type() == CHERRY || item[i].Type() == ICECREAM )
                                fruitCollected += 1;
                            else if ( item[i].Type() == EGG )
                                player[p].AddEggs();
                            bool foundText = false;
                            //Setup fancy text crap
                            for ( int j=0; j<MaxText; j++ )
                            {
                                if ( foundText == false )
                                {
                                    if ( txteffect[j].inUse == false )
                                    {
                                        if ( item[i].Type() == CHERRY || item[i].Type() == ICECREAM )
                                            txteffect[j].Setup( item[i].AdjustHPString().c_str(), item[i].X()+3, item[i].Y(), 100, 255, 75 );
                                        else if ( item[i].Type() == MONEY )
                                            txteffect[j].Setup( item[i].AdjustMoneyString().c_str(), item[i].X()+3, item[i].Y(), 255, 200, 0 );
                                        else if ( item[i].Type() == EGG )
                                            txteffect[j].Setup( item[i].AdjustEggString().c_str(), item[i].X()+3, item[i].Y(), 255, 255, 255 );
                                        foundText = true;
                                    }
                                }
                            }
                        }
                    }
                }

                for ( int i=0; i<level[0].EnemyCount() ; i++ )
                {
                    enemy[i].Update( player, soundTimer, soundTimer2, gameTimer, sndAttack, sndDamage, MaxText, txteffect );
                }

                //Check player stats
                if ( player[0].HP() <= 0 && player[1].HP() <= 0 )
                {
                    //game over
                    game.State( sGAMEOVER );
                }
                else if ( player[0].HP() <= 0 )
                {
                    player[0].Exists( false );
                    player[0].SetDeadTimer();
                }
                else if ( player[1].HP() <= 0 )
                {
                    player[1].Exists( false );
                    player[1].SetDeadTimer();
                }

            }//else if ( game.State() == sINGAME )

            if ( soundTimer > 0 ) { soundTimer -= 0.5; }
            if ( soundTimer2 > 0 ) { soundTimer2 -= 0.5; }
            gameTimer += 0.1;
            keyTimer -= 0.5;
            counter--;
		} // while counter > 0

        UpdateOffset( &offsetX, &offsetY, &player[0], &game );

        if ( game.State() == sINGAME )
        {
            game.BeginDraw();
            level[0].DrawBottomLayer( buffer, imgTileset, offsetX, offsetY );
            //Items
            for ( int i=0; i<level[0].ItemCount(); i++ )
            {
                if ( item[i].Type() == CHERRY )
                    item[i].Draw( buffer, imgCherry, offsetX, offsetY );
                else if ( item[i].Type() == ICECREAM )
                    item[i].Draw( buffer, imgIcecream, offsetX, offsetY );
                else if ( item[i].Type() == MONEY )
                    item[i].Draw( buffer, imgMoney, offsetX, offsetY );
                else if ( item[i].Type() == EGG )
                    item[i].Draw( buffer, imgEgg, offsetX, offsetY );
                item[i].Update();
            }


            int r = 0, g = 0, b = 0;
            for ( int ty = -1; ty < 2; ty++ )
            {
                for ( int tx = -1; tx < 2; tx++ )
                {
                    circle( buffer, mouse_x + tx, mouse_y + ty, 16, makecol( r, g, b ) );
                    rect( buffer, mouse_x-1 + tx, mouse_y-1 + ty, mouse_x+1 + tx, mouse_y+1 + ty, makecol( r, g, b ) );
                }
            }

            // Mouse Circle
            circle( buffer, mouse_x, mouse_y, 16, makecol( 0, 100, 255 ) );
            rect( buffer, mouse_x-1, mouse_y-1, mouse_x+1, mouse_y+1, makecol( 0, 100, 255 ) );

            //Player
            player[0].Draw( buffer, imgPlayer, offsetX, offsetY );
            player[1].Draw( buffer, imgPlayer2, offsetX, offsetY );

            //Enemies
            for ( int i=0; i<level[0].EnemyCount() ; i++ )
            {
                if ( i % 4 == 0 )
                    enemy[i].Draw( buffer, imgEwok, offsetX, offsetY );
                else if ( i % 4 == 1 )
                    enemy[i].Draw( buffer, imgRobot, offsetX, offsetY );
                else if ( i % 4 == 2 )
                    enemy[i].Draw( buffer, imgKitty, offsetX, offsetY );
                else
                    enemy[i].Draw( buffer, imgSnake, offsetX, offsetY );
            }
            level[0].DrawTopLayer( buffer, imgTileset, offsetX, offsetY );

            //basic HUD

            //Shadow
            int x, y;

            x = 150; y = 5;
            masked_blit( imgHudItem, buffer, 0, 0, x, y, 32, 32 );
            x += 40; y += 32;
            textprintf_ex( buffer, font, x-1, y+1, makecol( 0, 0, 0 ), -1, "%i", player[0].Monies() + player[1].Monies() );
            textprintf_ex( buffer, font, x, y, makecol( 255, 200, 0 ), -1, "%i", player[0].Monies() + player[1].Monies() );

            x += 100; y = 5;
            masked_blit( imgHudItem, buffer, 32, 0, x, y, 32, 32 );
            x += 40; y += 32;
            textprintf_ex( buffer, font, x-1, y+1, makecol( 0, 0, 0 ), -1, "%i", player[0].Eggs() + player[1].Eggs() );
            textprintf_ex( buffer, font, x, y, makecol( 75, 200, 75 ), -1, "%i", player[0].Eggs() + player[1].Eggs() );

            x += 100; y = 5;
            masked_blit( imgHudItem, buffer, 96, 0, x, y, 32, 32 );
            x += 40; y += 32;
            textprintf_ex( buffer, font, x-1, y+1, makecol( 0, 0, 0 ), -1, "%i %", fruitCollected );
            textprintf_ex( buffer, font, x, y, makecol( 200, 0, 255 ), -1, "%i %", fruitCollected );

            x += 100; y = 5;
            masked_blit( imgHudItem, buffer, 64, 0, x, y, 32, 32 );
            x += 40; y += 32;
            textprintf_ex( buffer, font, x-1, y+1, makecol( 0, 0, 0 ), -1, "%i", enemiesKilled );
            textprintf_ex( buffer, font, x, y, makecol( 200, 0, 0 ), -1, "%i", enemiesKilled  );

            //hp
            if ( player[0].Exists() )
            {
                x = player[0].X() - offsetX - 7;
                y = player[0].Y() - offsetY - 25;
                rectfill( buffer, x, y, x+(100/2), y+5, makecol( 20, 100, 20 ) );
                rectfill( buffer, x, y, x+(player[0].HP()/2), y+5, makecol( 0, 255, 0 ) );
                rect( buffer, x, y, x+(100/2), y+5, makecol( 0, 0, 0 ) );

                x = player[0].X() - offsetX - 7;
                y = player[0].Y() - offsetY - 15;
                rectfill( buffer, x, y, x+100/2, y+2, makecol( 25, 0, 0 ) );
                rectfill( buffer, x, y, x+(((float)player[0].Exp()/(float)player[0].ExpTilLevel())*100)/2, y+2, makecol( 240, 200, 0 ) );
            }

            if ( player[1].Exists() )
            {
                x = player[1].X() - offsetX - 7;
                y = player[1].Y() - offsetY - 25;
                rectfill( buffer, x, y, x+(100/2), y+5, makecol( 20, 100, 20 ) );
                rectfill( buffer, x, y, x+(player[1].HP()/2), y+5, makecol( 0, 255, 0 ) );
                rect( buffer, x, y, x+(100/2), y+5, makecol( 0, 0, 0 ) );

                x = player[1].X() - offsetX - 7;
                y = player[1].Y() - offsetY - 15;
                rectfill( buffer, x, y, x+100/2, y+2, makecol( 25, 0, 0 ) );
                rectfill( buffer, x, y, x+(((float)player[1].Exp()/(float)player[1].ExpTilLevel())*100)/2, y+2, makecol( 240, 200, 0 ) );
            }

            x = 10; y = 10;
            textprintf_ex( buffer, font, x-1, y+1, makecol( 0, 0, 0 ), -1, "PLAYER 1 LEVEL: %i", player[0].Level() );
            textprintf_ex( buffer, font, x, y, makecol( 255, 255, 255 ), -1, "PLAYER 1 LEVEL: %i", player[0].Level() );
            y += 10;
            textprintf_ex( buffer, font, x-1, y+1, makecol( 0, 0, 0 ), -1, "PLAYER 2 LEVEL: %i", player[1].Level() );
            textprintf_ex( buffer, font, x, y, makecol( 255, 255, 255 ), -1, "PLAYER 2 LEVEL: %i", player[1].Level() );


            x = 125;
            y = 450;
//            textprintf_ex( buffer, font, x-1, y+1, makecol( 0, 0, 0 ), -1, "Ask about the gamedev classes! ");

            y = 460;
            textprintf_ex( buffer, font, x-1, y+1, makecol( 0, 0, 0 ), -1, "Game by Rachel J Morris ");

            y -= 20;
            textprintf_ex( buffer, font, x-1, y+1, makecol( 0, 0, 0 ), -1, "Move: LEFT STICK or MOVE MOUSE");

            y -= 20;
            textprintf_ex( buffer, font, x-1, y+1, makecol( 0, 0, 0 ), -1, "Attack: (A) BUTTON or MOUSE BUTTON");

            for ( int i=0; i<5; i++ )
            {
                if ( txteffect[i].inUse )
                    txteffect[i].Draw( buffer, offsetX, offsetY );
            }

            if ( drawMiniMap )
            {
                DrawMiniMap( buffer, &level[0], &player[0], &player[1] );
            }

            //rect( buffer, 0, 0, 200, 200, makecol( 255, 0, 255 ) );

            game.EndDraw( buffer );
        }//draw in game
        else if ( game.State() == sMENU )
        {
            game.BeginDraw();
                draw_sprite( buffer, imgTitleScreen, 0, 0 );
                DrawMenuText( buffer );
            game.EndDraw( buffer );
        }
        else if ( game.State() == sGAMEOVER )
        {
            game.BeginDraw();
                draw_sprite( buffer, imgGameOver, 0, 0 );
            game.EndDraw( buffer );
        }
    }

    destroy_sample( sndPickup );
    destroy_sample( sndDamage );
    destroy_sample( sndConfirm );
    destroy_sample( sndDeath );
    destroy_sample( sndBack );
    destroy_sample( sndLevelUp );
    destroy_sample( song1 );
    destroy_bitmap( imgPlayer );
    destroy_bitmap( imgTileset );
    destroy_bitmap( imgHudItem );
    destroy_bitmap( imgBackground );
    destroy_bitmap( imgEwok );
    destroy_bitmap( imgSnake );
    destroy_bitmap( imgKitty );
    destroy_bitmap( imgRobot );
    destroy_bitmap( imgCherry );
    destroy_bitmap( imgIcecream );
    destroy_bitmap( imgMoney );
    destroy_bitmap( imgEgg );
    destroy_bitmap( buffer );
    destroy_bitmap( imgGameOver );

    cout << "THANK YOU" << endl;
    cout << "For playing Rawr Rinth by Moosader (Rachel J Morris)" << endl;

    return 0;
}
END_OF_MAIN();

void UpdateOffset( int *offsetX, int *offsetY, Character *player, Game *game )
{
    if (    ( player->X() - (int)( 0.5 * player->W() ) - (int)( 0.5 * game->ScreenWidth() ) >= 0 ) &&
            ( player->X() - (int)( 0.5 * player->W() ) - (int)( 0.5 * game->ScreenWidth() ) <= 1920 ) )
    {
        *offsetX = player->X() - (int)( 0.5 * player->W() ) - (int)( 0.5 * game->ScreenWidth() );
    }
    if (    ( player->Y() - (int)( 0.5 * player->H() ) - (int)( 0.5 * game->ScreenHeight() ) >= 0 ) &&
            ( player->Y() - (int)( 0.5 * player->H() ) - (int)( 0.5 * game->ScreenHeight() ) <= 1438 ) )
    {
        *offsetY = player->Y() - (int)( 0.5 * player->H() ) - (int)( 0.5 * game->ScreenHeight() );
    }
}

bool IsCollision( Character *character, Level *level, int dir )
{
    Rect player, tile[8];
    bool solid[8];
    player.x = character->RX();
    player.y = character->RY();
    player.w = character->RW();
    player.h = character->RH();

    if ( dir == UP )
    {
        player.y -= character->Speed();
    }
    else if ( dir == DOWN )
    {
        player.y += character->Speed();
    }
    else if ( dir == LEFT )
    {
        player.x -= character->Speed();
    }
    else if ( dir == RIGHT )
    {
        player.y += character->Speed();
    }

    //Check four surrounding tiles, on bottom and middle layers (top layer is ABOVE player, no solid ones)
    //layer 0, top-left tile
    //Set(x, y, w, h)
    tile[0].Set( level->tile[0][ player.x / 32 ][ player.y / 32 ].x, level->tile[0][ player.x / 32 ][ player.y / 32 ].y,
                    level->tile[0][ player.x / 32 ][ player.y / 32 ].w, level->tile[0][ player.x / 32 ][ player.y / 32 ].h );
    solid[0] = level->tile[0][ player.x / 32 ][ player.y / 32 ].solid;
    //layer 0, top-right tile
    tile[1].Set( level->tile[0][ player.x / 32 + 1 ][ player.y / 32 ].x, level->tile[0][ player.x / 32 + 1 ][ player.y / 32 ].y,
                    level->tile[0][ player.x / 32 + 1 ][ player.y / 32 ].w, level->tile[0][ player.x / 32 + 1 ][ player.y / 32 ].h );
    solid[1] = level->tile[0][ player.x / 32 + 1 ][ player.y / 32 ].solid;
    //layer 0, bottom-left tile
    tile[2].Set( level->tile[0][ player.x / 32 ][ player.y / 32 + 1 ].x, level->tile[0][ player.x / 32 ][ player.y / 32 + 1 ].y,
                    level->tile[0][ player.x / 32 ][ player.y / 32 + 1 ].w, level->tile[0][ player.x / 32 ][ player.y / 32 + 1 ].h );
    solid[2] = level->tile[0][ player.x / 32 ][ player.y / 32 + 1 ].solid;
    //layer 0, bottom-right tile
    tile[3].Set( level->tile[0][ player.x / 32 + 1 ][ player.y / 32 + 1 ].x, level->tile[0][ player.x / 32 + 1 ][ player.y / 32 + 1 ].y,
                    level->tile[0][ player.x / 32 + 1 ][ player.y / 32 + 1 ].w, level->tile[0][ player.x / 32 + 1 ][ player.y / 32 + 1 ].h );
    solid[3] = level->tile[0][ player.x / 32 + 1 ][ player.y / 32 + 1 ].solid;

    //layer 1, top-left tile
    tile[4].Set( level->tile[1][ player.x / 32 ][ player.y / 32 ].x, level->tile[1][ player.x / 32 ][ player.y / 32 ].y,
                    level->tile[1][ player.x / 32 ][ player.y / 32 ].w, level->tile[1][ player.x / 32 ][ player.y / 32 ].h );
    solid[4] = level->tile[1][ player.x / 32 ][ player.y / 32 ].solid;
    //layer 1, top-right tile
    tile[5].Set( level->tile[1][ player.x / 32 + 1 ][ player.y / 32 ].x, level->tile[1][ player.x / 32 + 1 ][ player.y / 32 ].y,
                    level->tile[1][ player.x / 32 + 1 ][ player.y / 32 ].w, level->tile[1][ player.x / 32 + 1 ][ player.y / 32 ].h );
    solid[5] = level->tile[1][ player.x / 32 + 1 ][ player.y / 32 ].solid;
    //layer 1, bottom-left tile
    tile[6].Set( level->tile[1][ player.x / 32 ][ player.y / 32 + 1 ].x, level->tile[1][ player.x / 32 ][ player.y / 32 + 1 ].y,
                    level->tile[1][ player.x / 32 ][ player.y / 32 + 1 ].w, level->tile[1][ player.x / 32 ][ player.y / 32 + 1 ].h );
    solid[6] = level->tile[1][ player.x / 32 ][ player.y / 32 + 1 ].solid;
    //layer 1, bottom-right tile
    tile[7].Set( level->tile[1][ player.x / 32 + 1 ][ player.y / 32 + 1 ].x, level->tile[1][ player.x / 32 + 1 ][ player.y / 32 + 1 ].y,
                    level->tile[1][ player.x / 32 + 1 ][ player.y / 32 + 1 ].w, level->tile[1][ player.x / 32 + 1 ][ player.y / 32 + 1 ].h );
    solid[7] = level->tile[1][ player.x / 32 + 1 ][ player.y / 32 + 1 ].solid;

    for ( int i=0; i<8; i++ )
    {
        if ( solid[i] == true && IsCollision( player, tile[i] ) )
            return true;
    }

    return false;
}

bool IsCollision( Enemy *character, Level *level, int dir )
{
    Rect player, tile[8];
    bool solid[8];
    player.x = character->RX();
    player.y = character->RY();
    player.w = character->RW();
    player.h = character->RH();

    if ( dir == UP )
    {
        player.y -= character->Speed();
    }
    else if ( dir == DOWN )
    {
        player.y += character->Speed();
    }
    else if ( dir == LEFT )
    {
        player.x -= character->Speed();
    }
    else if ( dir == RIGHT )
    {
        player.y += character->Speed();
    }

    //Check four surrounding tiles, on bottom and middle layers (top layer is ABOVE player, no solid ones)
    //layer 0, top-left tile
    //Set(x, y, w, h)
    tile[0].Set( level->tile[0][ player.x / 32 ][ player.y / 32 ].x, level->tile[0][ player.x / 32 ][ player.y / 32 ].y,
                    level->tile[0][ player.x / 32 ][ player.y / 32 ].w, level->tile[0][ player.x / 32 ][ player.y / 32 ].h );
    solid[0] = level->tile[0][ player.x / 32 ][ player.y / 32 ].solid;
    //layer 0, top-right tile
    tile[1].Set( level->tile[0][ player.x / 32 + 1 ][ player.y / 32 ].x, level->tile[0][ player.x / 32 + 1 ][ player.y / 32 ].y,
                    level->tile[0][ player.x / 32 + 1 ][ player.y / 32 ].w, level->tile[0][ player.x / 32 + 1 ][ player.y / 32 ].h );
    solid[1] = level->tile[0][ player.x / 32 + 1 ][ player.y / 32 ].solid;
    //layer 0, bottom-left tile
    tile[2].Set( level->tile[0][ player.x / 32 ][ player.y / 32 + 1 ].x, level->tile[0][ player.x / 32 ][ player.y / 32 + 1 ].y,
                    level->tile[0][ player.x / 32 ][ player.y / 32 + 1 ].w, level->tile[0][ player.x / 32 ][ player.y / 32 + 1 ].h );
    solid[2] = level->tile[0][ player.x / 32 ][ player.y / 32 + 1 ].solid;
    //layer 0, bottom-right tile
    tile[3].Set( level->tile[0][ player.x / 32 + 1 ][ player.y / 32 + 1 ].x, level->tile[0][ player.x / 32 + 1 ][ player.y / 32 + 1 ].y,
                    level->tile[0][ player.x / 32 + 1 ][ player.y / 32 + 1 ].w, level->tile[0][ player.x / 32 + 1 ][ player.y / 32 + 1 ].h );
    solid[3] = level->tile[0][ player.x / 32 + 1 ][ player.y / 32 + 1 ].solid;

    //layer 1, top-left tile
    tile[4].Set( level->tile[1][ player.x / 32 ][ player.y / 32 ].x, level->tile[1][ player.x / 32 ][ player.y / 32 ].y,
                    level->tile[1][ player.x / 32 ][ player.y / 32 ].w, level->tile[1][ player.x / 32 ][ player.y / 32 ].h );
    solid[4] = level->tile[1][ player.x / 32 ][ player.y / 32 ].solid;
    //layer 1, top-right tile
    tile[5].Set( level->tile[1][ player.x / 32 + 1 ][ player.y / 32 ].x, level->tile[1][ player.x / 32 + 1 ][ player.y / 32 ].y,
                    level->tile[1][ player.x / 32 + 1 ][ player.y / 32 ].w, level->tile[1][ player.x / 32 + 1 ][ player.y / 32 ].h );
    solid[5] = level->tile[1][ player.x / 32 + 1 ][ player.y / 32 ].solid;
    //layer 1, bottom-left tile
    tile[6].Set( level->tile[1][ player.x / 32 ][ player.y / 32 + 1 ].x, level->tile[1][ player.x / 32 ][ player.y / 32 + 1 ].y,
                    level->tile[1][ player.x / 32 ][ player.y / 32 + 1 ].w, level->tile[1][ player.x / 32 ][ player.y / 32 + 1 ].h );
    solid[6] = level->tile[1][ player.x / 32 ][ player.y / 32 + 1 ].solid;
    //layer 1, bottom-right tile
    tile[7].Set( level->tile[1][ player.x / 32 + 1 ][ player.y / 32 + 1 ].x, level->tile[1][ player.x / 32 + 1 ][ player.y / 32 + 1 ].y,
                    level->tile[1][ player.x / 32 + 1 ][ player.y / 32 + 1 ].w, level->tile[1][ player.x / 32 + 1 ][ player.y / 32 + 1 ].h );
    solid[7] = level->tile[1][ player.x / 32 + 1 ][ player.y / 32 + 1 ].solid;

    for ( int i=0; i<8; i++ )
    {
        if ( solid[i] == true && IsCollision( player, tile[i] ) )
            return true;
    }

    return false;
}

bool IsCollision( Character *player, Item *item )
{
    Rect pl, it;
    pl.x = player->RX();
    pl.y = player->RY();
    pl.w = player->RW();
    pl.h = player->RH();

    it.x = item->RX();
    it.y = item->RY();
    it.w = item->RW();
    it.h= item->RH();

    return IsCollision( pl, it );
}

bool IsCollision( Character *player, Enemy *enemy )
{
    Rect pl, en;
    pl.x = player->RX();
    pl.y = player->RY();
    pl.w = player->RW();
    pl.h = player->RH();

    en.x = enemy->RX();
    en.y = enemy->RY();
    en.w = enemy->RW();
    en.h= enemy->RH();

    return IsCollision( pl, en );
}

void IncrementCounter()
{
    counter++;
}

bool IsCollision( Rect r1, Rect r2 )
{
    if (    r1.x            <       r2.x+r2.w &&
            r1.x+r1.w    >       r2.x &&
            r1.y            <       r2.y+r2.h &&
            r1.y+r1.h     >       r2.y )
    {
        return true;
    }
    return false;
}

float Distance( int x1, int y1, int x2, int y2 )
{
    //a^2 + b^2 = c^2
    return ( sqrt ( (x2 - x1)*(x2 - x1) + (y2 - y1)*(y2 - y1) ) );
}

void DrawMenuText( BITMAP *buffer )
{
    int x = 520, y = 220;

                //shadow
                textprintf_centre_ex( buffer, font, x-1, y+1, makecol( 0, 0, 0 ), -1, "Press ENTER to start" );
                textprintf_centre_ex( buffer, font, x, y+1, makecol( 0, 0, 0 ), -1, "Press ENTER to start" );
                textprintf_centre_ex( buffer, font, x+1, y+1, makecol( 0, 0, 0 ), -1, "Press ENTER to start" );

                textprintf_centre_ex( buffer, font, x-1, y, makecol( 0, 0, 0 ), -1, "Press ENTER to start" );
                textprintf_centre_ex( buffer, font, x+1, y, makecol( 0, 0, 0 ), -1, "Press ENTER to start" );

                textprintf_centre_ex( buffer, font, x-1, y-1, makecol( 0, 0, 0 ), -1, "Press ENTER to start" );
                textprintf_centre_ex( buffer, font, x, y-1, makecol( 0, 0, 0 ), -1, "Press ENTER to start" );
                textprintf_centre_ex( buffer, font, x+1, y-1, makecol( 0, 0, 0 ), -1, "Press ENTER to start" );

                //gold text
                textprintf_centre_ex( buffer, font, x, y, makecol( 255, 200, 0 ), -1, "Press ENTER to start" );

                y = 230;

                //shadow
                textprintf_centre_ex( buffer, font, x-1, y+1, makecol( 0, 0, 0 ), -1, "or F4 to quit" );
                textprintf_centre_ex( buffer, font, x, y+1, makecol( 0, 0, 0 ), -1, "or F4 to quit" );
                textprintf_centre_ex( buffer, font, x+1, y+1, makecol( 0, 0, 0 ), -1, "or F4 to quit" );

                textprintf_centre_ex( buffer, font, x-1, y, makecol( 0, 0, 0 ), -1, "or F4 to quit" );
                textprintf_centre_ex( buffer, font, x+1, y, makecol( 0, 0, 0 ), -1, "or F4 to quit" );

                textprintf_centre_ex( buffer, font, x-1, y-1, makecol( 0, 0, 0 ), -1, "or F4 to quit" );
                textprintf_centre_ex( buffer, font, x, y-1, makecol( 0, 0, 0 ), -1, "or F4 to quit" );
                textprintf_centre_ex( buffer, font, x+1, y-1, makecol( 0, 0, 0 ), -1, "or F4 to quit" );

                //gold text
                textprintf_centre_ex( buffer, font, x, y, makecol( 255, 200, 0 ), -1, "or F4 to quit" );
}

void DrawMiniMap( BITMAP *buffer, Level *level, Character *player, Character *player2 )
{
    int x = 8;
    int y = 412;
    for ( int i=0; i<TILEX; i++ )
    {
        for ( int j=0; j<TILEY; j++ )
        {
            if ( level->tile[0][i][j].solid )
                putpixel( buffer, i+x, j+y, makecol( 255, 255, 255 ) );
            if ( level->tile[1][i][j].solid )
                putpixel( buffer, i+x, j+y, makecol( 255, 255, 0 ) );

            putpixel( buffer, (player->X()/32) + x,      (player->Y()/32) + y-1, makecol( 255, 0, 0 ) );//up
            putpixel( buffer, (player->X()/32) + x,      (player->Y()/32) + y+1, makecol( 255, 0, 0 ) );//down
            putpixel( buffer, (player->X()/32) + x-1,    (player->Y()/32) + y, makecol( 255, 0, 0 ) );//left
            putpixel( buffer, (player->X()/32) + x+1,    (player->Y()/32) + y, makecol( 255, 0, 0 ) );//right
            putpixel( buffer, (player->X()/32) + x,      (player->Y()/32) + y, makecol( 255, 0, 0 ) );//center

            putpixel( buffer, (player2->X()/32) + x,      (player2->Y()/32) + y-1, makecol( 0, 0, 255 ) );//up
            putpixel( buffer, (player2->X()/32) + x,      (player2->Y()/32) + y+1, makecol( 0, 0, 255 ) );//down
            putpixel( buffer, (player2->X()/32) + x-1,    (player2->Y()/32) + y, makecol( 0, 0, 255 ) );//left
            putpixel( buffer, (player2->X()/32) + x+1,    (player2->Y()/32) + y, makecol( 0, 0, 255 ) );//right
            putpixel( buffer, (player2->X()/32) + x,      (player2->Y()/32) + y, makecol( 0, 0, 255 ) );//center

        }
    }
}




