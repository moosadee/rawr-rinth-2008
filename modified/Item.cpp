#include "Item.h"

Item::Item()
{
    frame = 0;
    x = y = w = h = 0;
    exists = false;
    HPAdd = "0";
}

void Item::Setup( int i )
{
    index = i;
    exists = true;
    frame = 0;
    int mod = 8;
    addToHP = 0;
    addToScore = 0;
    addToMoney = 0;
    HPAdd = " NONE ";
    MoneyAdd = "NULL";
    if ( i % mod == 0 || i % mod == 1 )
    {
        type = CHERRY;
        w = h = 32;
        FRAMEMAX = 4;
        addToScore = 10;
        addToHP = 10;
        HPAdd = "+10";
    }
    else if ( i % mod == 2 || i % mod == 3 )
    {
        type = ICECREAM;
        w = h = 32;
        FRAMEMAX = 4;
        addToScore = 10;
        addToHP = 25;
        HPAdd = "+25";
    }
    else if ( i % mod == 4 )
    {
        type = EGG;
        w = h = 32;
        FRAMEMAX = 2;
        addToScore = 100;
    }
    else if ( i % mod == 5 )
    {
        type = MONEY;
        w = h = 32;
        FRAMEMAX = 1;
        addToScore = 20;
        addToMoney = 10;
        MoneyAdd = "+10";
    }
    else if ( i % mod == 6 )
    {
        type = MONEY;
        w = h = 32;
        FRAMEMAX = 1;
        addToScore = 50;
        addToMoney = 25;
        MoneyAdd = "+25";
    }
    else
    {
        type = MONEY;
        w = h = 32;
        FRAMEMAX = 1;
        addToScore = 60;
        addToMoney = 30;
        MoneyAdd = "+30";
    }

    int tile = 32;  //tile width/height
    switch( i )
    {
        case 0:
            x = 54 * tile;
            y = 13 * tile;
        break;
        case 1:
            x = 9 * tile;
            y = 3 * tile;
        break;
        case 2:
            x = 74 * tile;
            y = 43 * tile;
        break;
        case 3:
            x = 54 * tile;
            y = 34 * tile;
        break;
        case 4:
            x = 23 * tile;
            y = 47 * tile;
        break;
        case 5:
            x = 11 * tile;
            y = 14 * tile;
        break;
        case 6:
            x = 59 * tile;
            y = 23 * tile;
        break;
        case 7:
            x = 45 * tile;
            y = 18 * tile;
        break;
        case 8:
            x = 44 * tile;
            y = 53 * tile;
        break;
        case 9:
            x = 7 * tile;
            y = 29 * tile;
        break;
        case 10:
            x = 71 * tile;
            y = 41 * tile;
        break;
        case 11:
            x = 78 * tile;
            y = 26 * tile;
        break;
        case 12:
            x = 29 * tile;
            y = 9 * tile;
        break;
        case 13:
            x = 28;
            y = 969;
        break;
        case 14:
            x = 99;
            y = 1231;
        break;
        case 15:
            x = 477;
            y = 1567;
        break;
        case 16:
            x = 921;
            y = 1220;
        break;
        case 17:
            x = 609;
            y = 602;
        break;
        case 18:
            x = 2272;
            y = 842;
        break;
        case 19:
            x = 2064;
            y = 1747;
        break;
        case 20:
            x = 1619;
            y = 259;
        break;
        case 21:
            x = 1211;
            y = 112;
        break;
        case 22:
            x = 521;
            y = 121;
        break;
        case 23:
            x = 2129;
            y = 208;
        break;
        case 24:
            x = 2387;
            y = 492;
        break;
        case 25:
            x = 1021;
            y = 716;
        break;
        case 26:
            x = 1375;
            y = 1284;
        break;
        case 27:
            x = 2171;
            y = 1148;
        break;
        case 28:
            x = 1602;
            y = 1764;
        break;
        case 29:
            x = 2464;
            y = 1747;
        break;
    }
    region.x = x + 5;
    region.y = y + 5;
    region.w = w/2 - 10;
    region.h = h/2 - 10;
}

void Item::Update()
{
    region.x = x + 5;
    region.y = y + 5;
    region.w = w/2 - 10;
    region.h = h/2 - 10;
    IncrementFrame();
}

