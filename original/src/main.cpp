#include <iostream>
#include <math.h>
#include "allegro.h"
#include "Character.h"
#include "Game.h"
#include "Level.h"
#include "Item.h"
#include "TextEffect.h"
using namespace std;

void UpdateOffset( int *offsetX, int *offsetY, Character *player, Game *game );
bool IsCollision( Character *player, Level *level, int dir );
volatile long counter = 0;
void IncrementCounter();
bool IsCollision( Rect r1, Rect r2 );
bool IsCollision( Character *player, Level *level, int dir );
bool IsCollision( Character *player, Item *item );
bool IsCollision( Character *player, Enemy *enemy );
float Distance( int x1, int y1, int x2, int y2 );
void DrawMenuText( BITMAP *buffer );
void TextAnimation( BITMAP *buffer, string text, float counter );
void DrawMiniMap( BITMAP *buffer, Level *level, Character *player );

/*
RAWR RINTH
v 0.4 - game prototype
By Rachel J. Morris
http://www.moosader.com

Messy code, yay!
*/

const int MaxText = 20;     //Maximum amount of text on screen at once (too lazy to use dynamic array!)

const int VIEWDIST = 250;   //distance enemies can see

int main()
{
    Game game;

    //Timer stuff handles FPS regulation
    LOCK_VARIABLE( counter );
    LOCK_FUNCTION( IncrementCounter );
    install_int_ex( IncrementCounter,BPS_TO_TIMER(90) );

    //These probably should go somewhere else.  Oh well.
    int fruitCollected = 0, enemiesKilled = 0;
    int offsetX = 0, offsetY = 0, frame = 0;
    float gameTimer = 0.0;
    float soundTimer = 0, soundTimer2 = 0;
    bool drawMiniMap = true;

    //Make sure there's a pause between key repeats for toggles and such.
    float keyTimer = 0;

    TextEffect txteffect[MaxText];

    Level level[1];
    level[0].LoadMap( "lv-00.map" );
    //Sounds
    SAMPLE *sndPickup = load_sample( "ding.wav" );
    SAMPLE *sndAttack = load_sample( "attack.wav" );
    SAMPLE *sndDamage = load_sample( "explosion.wav" );
    SAMPLE *sndConfirm = load_sample( "sfx2.wav" );
    SAMPLE *sndDeath = load_sample( "death.wav" );
    SAMPLE *sndLevelUp = load_sample( "levelup.wav" );
    SAMPLE *sndBack = load_sample( "sfx.wav" );
    SAMPLE *song1 = load_sample( "Dinosaur.wav" );
    //Graphics
    BITMAP *imgTileset = load_bitmap( "tileset2.bmp", NULL );
    BITMAP *imgPlayer = load_bitmap( "rawr.bmp", NULL );
    BITMAP *imgRobot = load_bitmap( "robot.bmp", NULL );
    BITMAP *imgEwok = load_bitmap( "ewok.bmp", NULL );
    BITMAP *imgSnake = load_bitmap( "snake.bmp", NULL );
    BITMAP *imgKitty = load_bitmap( "kitty.bmp", NULL );
    BITMAP *imgBackground = load_bitmap( "bg.bmp", NULL );
    BITMAP *imgCherry = load_bitmap( "cherry.bmp", NULL );
    BITMAP *imgIcecream = load_bitmap( "icecream.bmp", NULL );
    BITMAP *imgEgg = load_bitmap( "egg.bmp", NULL );
    BITMAP *imgMoney = load_bitmap( "coins.bmp", NULL );
    BITMAP *imgHudItem = load_bitmap( "items.bmp", NULL );
    BITMAP *imgTitleScreen = load_bitmap( "title.bmp", NULL );
    BITMAP *imgGameOver = load_bitmap( "gameover.bmp", NULL );
    BITMAP *buffer = create_bitmap( game.ScreenWidth(), game.ScreenHeight() );

    Enemy enemy[ level[0].EnemyCount() ];
    for ( int i=0; i<level[0].EnemyCount() ; i++ )      //Init enemies based on index
    {
        enemy[i].Setup( i );
    }
    Item item[ level[0].ItemCount() ];
    for ( int i=0; i<level[0].ItemCount(); i++ )        //Init items based on index
    {
        item[i].Setup( i );
    }

    Character player;

    game.State( sMENU );        //Game begins in menu
    while ( game.Done() == false )
    {
        while ( counter > 0 )   //Regulate FPS
        {
        if ( key[KEY_F4] ) { game.Done( true ); }       //Quit button
            //Menu
            if ( game.State() == sMENU )
            {
                if ( key[KEY_ENTER] )
                {
                    play_sample( sndConfirm, 255, 128, 1000, false );
                    game.State( sINGAME );
                    stop_sample( song1 );
                    play_sample( song1, 200, 128, 1000, true );
                }
            } // end menu
            if ( key[KEY_F5] )
            {
                game.ToggleFullscreen();
            }
            //Game
            else if ( game.State() == sINGAME )
            {
                if ( key[KEY_ESC] )
                {
                    play_sample( sndBack, 255, 128, 1000, false );
                    game.State( sMENU );
                    stop_sample( song1 );
                }
                if ( key[KEY_F8] && keyTimer <= 0 )
                {
                    cout<<"\n\nPlayer Coordinates: ( "<<player.X()<<", "<<player.Y()<<" )"<<endl;
                    cout<<"Offset: ( "<<offsetX<<", "<<offsetY<<" )"<<endl;
                    cout<<"Exp: "<<player.Exp()<<endl;
                    cout<<"Level: "<<player.Level()<<endl;
                    cout<<"Exp until next level: "<<player.ExpTilLevel()<<endl;
                    keyTimer = 10;
                }
                if ( key[KEY_F9] && keyTimer <= 0 )
                {
                    cout<<"( "<<player.X()<<", "<<player.Y()<<" )"<<endl<<endl;
                    keyTimer = 10;
                }
                else if ( key[KEY_M] && keyTimer <= 0 )
                {
                    if ( drawMiniMap ) { drawMiniMap = false; keyTimer = 10; }
                    else { drawMiniMap = true; keyTimer = 10; }
                }
                //PLAYER MOVEMENT
                if ( key[KEY_UP] || key[KEY_W] )
                {
                    if ( (key[KEY_RSHIFT] || key[KEY_LSHIFT]) && player.Stamina() > 0 ) { player.Speed( RUN ); }
                    else { player.Speed( WALK ); }
                    if ( IsCollision( &player, &level[0], UP) == false ) { player.Move( UP ); }
                }
                else if ( key[KEY_DOWN] || key[KEY_S] )
                {
                    if ( (key[KEY_RSHIFT] || key[KEY_LSHIFT]) && player.Stamina() > 0 ) { player.Speed( RUN ); }
                    else { player.Speed( WALK ); }
                    if ( IsCollision( &player, &level[0], DOWN) == false ) { player.Move( DOWN ); }
                }
                if ( key[KEY_LEFT] || key[KEY_A] )
                {
                    if ( (key[KEY_RSHIFT] || key[KEY_LSHIFT]) && player.Stamina() > 0 ) { player.Speed( RUN ); }
                    else { player.Speed( WALK ); }
                    if ( IsCollision( &player, &level[0], LEFT ) == false ) { player.Move( LEFT ); }
                }
                else if ( key[KEY_RIGHT] || key[KEY_D] )
                {
                    if ( (key[KEY_RSHIFT] || key[KEY_LSHIFT]) && player.Stamina() > 0 ) { player.Speed( RUN ); }
                    else { player.Speed( WALK ); }
                    if ( IsCollision( &player, &level[0], RIGHT) == false ) { player.Move( RIGHT ); }

                }

                //drain stamina
                if ( (key[KEY_RSHIFT] || key[KEY_LSHIFT]) && ( key[KEY_UP] || key[KEY_DOWN] || key[KEY_LEFT] || key[KEY_RIGHT]
                                                        || key[KEY_W] || key[KEY_S] || key[KEY_A] || key[KEY_D] ) )
                {
                    player.DrainStamina();
                }
                else if ( !(key[KEY_RSHIFT] || key[KEY_LSHIFT]) )
                {
                    player.RestoreStamina();
                }

                if ( key[KEY_SPACE] || key[KEY_RCONTROL] )
                {
                    //player attacks
                    player.BeginAttack();
                }
                if ( key[KEY_F5] )
                {
                    game.ToggleFullscreen();
                }

                //Update enemies
                for ( int i=0; i<level[0].EnemyCount() ; i++ )
                {
                    if ( Distance( enemy[i].X(), enemy[i].Y(), player.X(), player.Y() ) <= VIEWDIST )
                    {
                        //chase player
                        if ( (int)gameTimer % 3 == 0 )
                        {
                            if ( player.Y() < enemy[i].Y() && IsCollision( &enemy[i], &level[0], UP ) == false )
                            {
                                enemy[i].Move( UP );
                            }
                            else if ( player.Y() > enemy[i].Y() && IsCollision( &enemy[i], &level[0], DOWN ) == false )
                            {
                                enemy[i].Move( DOWN );
                            }
                        }
                        else
                        {
                            if ( player.X() < enemy[i].X() && IsCollision( &enemy[i], &level[0], LEFT ) == false )
                            {
                                enemy[i].Move( LEFT );
                            }
                            else if ( player.X() > enemy[i].X() && IsCollision( &enemy[i], &level[0], RIGHT ) == false )
                            {
                                enemy[i].Move( RIGHT );
                            }
                        }
                    }
                }
                player.Update();

                for ( int i=0; i<MaxText; i++ )       //update text animation
                {
                    if ( txteffect[i].inUse )
                        txteffect[i].Update();
                }

                //Check collisions
                for ( int i=0; i<level[0].ItemCount(); i++ )
                {
                    if ( item[i].Exists() == true && IsCollision( &player, &item[i] ) )
                    {
                        item[i].Exists( false );
                        player.AddScore( item[i].AdjustScore() );
                        player.AddHP( item[i].AdjustHP() );
                        player.AddMonies( item[i].AdjustMoney() );
                        play_sample( sndPickup, 255, 128, 1000, false );
                        if ( item[i].Type() == CHERRY || item[i].Type() == ICECREAM )
                            fruitCollected += 1;
                        else if ( item[i].Type() == EGG )
                            player.AddEggs();
                        bool foundText = false;
                        //Setup fancy text crap
                        for ( int j=0; j<MaxText; j++ )
                        {
                            if ( foundText == false )
                            {
                                if ( txteffect[j].inUse == false )
                                {
                                    if ( item[i].Type() == CHERRY || item[i].Type() == ICECREAM )
                                        txteffect[j].Setup( item[i].AdjustHPString().c_str(), item[i].X()+3, item[i].Y(), 100, 255, 75 );
                                    else if ( item[i].Type() == MONEY )
                                        txteffect[j].Setup( item[i].AdjustMoneyString().c_str(), item[i].X()+3, item[i].Y(), 255, 200, 0 );
                                    else if ( item[i].Type() == EGG )
                                        txteffect[j].Setup( item[i].AdjustEggString().c_str(), item[i].X()+3, item[i].Y(), 255, 255, 255 );
                                    foundText = true;
                                }
                            }
                        }
                    }
                }
                for ( int i=0; i<level[0].EnemyCount() ; i++ )
                {
                    if ( enemy[i].Exists() == true && enemy[i].HP() <= 0 )      //ENEMY DIED
                    {
                        play_sample( sndDeath, 255, 128, 1000, false );
                        enemiesKilled += 1;

                        enemy[i].Exists( false );
                        if ( player.Exp( enemy[i].GivesExp() ) )
                        {
                            //level up
                            play_sample( sndLevelUp, 255, 128, 1000, false );
                        }
                    }

                    if ( enemy[i].Exists() == true && IsCollision( &player, &enemy[i] ) )
                    {
                        if ( player.Is() == ATTACKING )
                        {
                            if ( soundTimer <= 0 )
                            {
                                play_sample( sndAttack, 255, 128, 1000, false );
                                soundTimer = 5.0;
                                enemy[i].AddHP( player.StrengthFloat() );
                                bool foundText = false;
                                //Setup fancy text crap
                                for ( int j=0; j<MaxText; j++ )
                                {
                                    if ( foundText == false )
                                    {
                                        if ( txteffect[j].inUse == false )
                                        {
                                            txteffect[j].Setup( player.StrengthString().c_str(), enemy[i].X()+3, enemy[i].Y(), 255, 100, 75 );
                                            foundText = true;
                                        }
                                    }
                                }
                            }
                        }//attacking

                        //enemy attack
                        if ( soundTimer2 <= 0 && gameTimer > 20 )
                        {
                            play_sample( sndDamage, 255, 128, 1000, false );
                            soundTimer2 = 10.0;
                            player.AddHP( -2 );
                            bool foundText = false;
                            //Setup fancy text crap
                            for ( int j=0; j<MaxText; j++ )
                            {
                                if ( foundText == false )
                                {
                                    if ( txteffect[j].inUse == false )
                                    {
                                        txteffect[j].Setup( "-2", player.X()+3, player.Y(), 255, 0, 0 );
                                        foundText = true;
                                    }
                                }
                            }
                        }//not attacking
                    }//if ( enemy[i].Exists() == true && IsCollision( &player, &enemy[i] ) )
                }

                //Check player stats
                if ( player.HP() <= 0 )
                {
                    //game over
                    game.State( sGAMEOVER );
                }

            }//else if ( game.State() == sINGAME )

            if ( soundTimer > 0 ) { soundTimer -= 0.5; }
            if ( soundTimer2 > 0 ) { soundTimer2 -= 0.5; }
            gameTimer += 0.1;
            keyTimer -= 0.5;
            counter--;
		} // while counter > 0

        UpdateOffset( &offsetX, &offsetY, &player, &game );

        if ( game.State() == sINGAME )
        {
            game.BeginDraw();
                level[0].DrawBottomLayer( buffer, imgTileset, offsetX, offsetY );
                //Items
                for ( int i=0; i<level[0].ItemCount(); i++ )
                {
                    if ( item[i].Type() == CHERRY )
                        item[i].Draw( buffer, imgCherry, offsetX, offsetY );
                    else if ( item[i].Type() == ICECREAM )
                        item[i].Draw( buffer, imgIcecream, offsetX, offsetY );
                    else if ( item[i].Type() == MONEY )
                        item[i].Draw( buffer, imgMoney, offsetX, offsetY );
                    else if ( item[i].Type() == EGG )
                        item[i].Draw( buffer, imgEgg, offsetX, offsetY );
                    item[i].Update();
                }
                //Player
                player.Draw( buffer, imgPlayer, offsetX, offsetY );
                //Enemies
                for ( int i=0; i<level[0].EnemyCount() ; i++ )
                {
                    if ( i % 4 == 0 )
                        enemy[i].Draw( buffer, imgEwok, offsetX, offsetY );
                    else if ( i % 4 == 1 )
                        enemy[i].Draw( buffer, imgRobot, offsetX, offsetY );
                    else if ( i % 4 == 2 )
                        enemy[i].Draw( buffer, imgKitty, offsetX, offsetY );
                    else
                        enemy[i].Draw( buffer, imgSnake, offsetX, offsetY );
                    enemy[i].Update();
                }
                level[0].DrawTopLayer( buffer, imgTileset, offsetX, offsetY );

                //basic HUD

                //Shadow
                int x, y;

                x = 150; y = 5;
                masked_blit( imgHudItem, buffer, 0, 0, x, y, 32, 32 );
                x += 40; y += 32;
                textprintf_ex( buffer, font, x-1, y+1, makecol( 0, 0, 0 ), -1, "%i", player.Monies() );
                textprintf_ex( buffer, font, x, y, makecol( 255, 200, 0 ), -1, "%i", player.Monies() );

                x += 100; y = 5;
                masked_blit( imgHudItem, buffer, 32, 0, x, y, 32, 32 );
                x += 40; y += 32;
                textprintf_ex( buffer, font, x-1, y+1, makecol( 0, 0, 0 ), -1, "%i", player.Eggs() );
                textprintf_ex( buffer, font, x, y, makecol( 75, 200, 75 ), -1, "%i", player.Eggs() );

                x += 100; y = 5;
                masked_blit( imgHudItem, buffer, 96, 0, x, y, 32, 32 );
                x += 40; y += 32;
                textprintf_ex( buffer, font, x-1, y+1, makecol( 0, 0, 0 ), -1, "%i %", fruitCollected );
                textprintf_ex( buffer, font, x, y, makecol( 200, 0, 255 ), -1, "%i %", fruitCollected );

                x += 100; y = 5;
                masked_blit( imgHudItem, buffer, 64, 0, x, y, 32, 32 );
                x += 40; y += 32;
                textprintf_ex( buffer, font, x-1, y+1, makecol( 0, 0, 0 ), -1, "%i", enemiesKilled );
                textprintf_ex( buffer, font, x, y, makecol( 200, 0, 0 ), -1, "%i", enemiesKilled  );

                //hp
                x = 10; y = 10;
                textprintf_ex( buffer, font, x-1, y+1, makecol( 0, 0, 0 ), -1, "HP: ");
                textprintf_ex( buffer, font, x, y, makecol( 255, 255, 255 ), -1, "HP: ");
                x += 35; y += 1;
                rectfill( buffer, x, y, x+100, y+5, makecol( 20, 100, 20 ) );
                rectfill( buffer, x, y, x+player.HP(), y+5, makecol( 0, 255, 0 ) );
                rect( buffer, x, y, x+100, y+5, makecol( 0, 0, 0 ) );

                x = 10; y += 15;
                textprintf_ex( buffer, font, x-1, y+1, makecol( 0, 0, 0 ), -1, "EXP: ");
                textprintf_ex( buffer, font, x, y, makecol( 255, 255, 255 ), -1, "EXP: ");
                x += 35; y += 1;
                rectfill( buffer, x, y, x+100, y+5, makecol( 25, 0, 0 ) );
                rectfill( buffer, x, y, x+(((float)player.Exp()/(float)player.ExpTilLevel())*100), y+5, makecol( 240, 200, 0 ) );
                rect( buffer, x, y, x+100, y+5, makecol( 0, 0, 0 ) );

                x = 10; y +=15;
                textprintf_ex( buffer, font, x-1, y+1, makecol( 0, 0, 0 ), -1, "RUN: ");
                textprintf_ex( buffer, font, x, y, makecol( 255, 255, 255 ), -1, "RUN: ");
                x += 35; y += 1;
                rectfill( buffer, x, y, x+100, y+5, makecol( 25, 0, 0 ) );
                rectfill( buffer, x, y, x+player.Stamina(), y+5, makecol( 0, 200, 255 ) );
                rect( buffer, x, y, x+100, y+5, makecol( 0, 0, 0 ) );

                x = 10; y = 65;
                textprintf_ex( buffer, font, x-1, y+1, makecol( 0, 0, 0 ), -1, "LEVEL: %i", player.Level() );
                textprintf_ex( buffer, font, x, y, makecol( 255, 255, 255 ), -1, "LEVEL: %i", player.Level() );

                for ( int i=0; i<5; i++ )
                {
                    if ( txteffect[i].inUse )
                        txteffect[i].Draw( buffer, offsetX, offsetY );
                }

                if ( drawMiniMap )
                {
                    DrawMiniMap( buffer, &level[0], &player );
                }
            game.EndDraw( buffer );
        }//draw in game
        else if ( game.State() == sMENU )
        {
            game.BeginDraw();
                draw_sprite( buffer, imgTitleScreen, 0, 0 );
                DrawMenuText( buffer );
            game.EndDraw( buffer );
        }
        else if ( game.State() == sGAMEOVER )
        {
            game.BeginDraw();
                draw_sprite( buffer, imgGameOver, 0, 0 );
            game.EndDraw( buffer );
        }
    }

    destroy_sample( sndPickup );
    destroy_sample( sndDamage );
    destroy_sample( sndConfirm );
    destroy_sample( sndDeath );
    destroy_sample( sndBack );
    destroy_sample( sndLevelUp );
    destroy_sample( song1 );
    destroy_bitmap( imgPlayer );
    destroy_bitmap( imgTileset );
    destroy_bitmap( imgHudItem );
    destroy_bitmap( imgBackground );
    destroy_bitmap( imgEwok );
    destroy_bitmap( imgSnake );
    destroy_bitmap( imgKitty );
    destroy_bitmap( imgRobot );
    destroy_bitmap( imgCherry );
    destroy_bitmap( imgIcecream );
    destroy_bitmap( imgMoney );
    destroy_bitmap( imgEgg );
    destroy_bitmap( buffer );
    destroy_bitmap( imgGameOver );
    return 0;
}
END_OF_MAIN();

void UpdateOffset( int *offsetX, int *offsetY, Character *player, Game *game )
{
    if (    ( player->X() - (int)( 0.5 * player->W() ) - (int)( 0.5 * game->ScreenWidth() ) >= 0 ) &&
            ( player->X() - (int)( 0.5 * player->W() ) - (int)( 0.5 * game->ScreenWidth() ) <= 1920 ) )
    {
        *offsetX = player->X() - (int)( 0.5 * player->W() ) - (int)( 0.5 * game->ScreenWidth() );
    }
    if (    ( player->Y() - (int)( 0.5 * player->H() ) - (int)( 0.5 * game->ScreenHeight() ) >= 0 ) &&
            ( player->Y() - (int)( 0.5 * player->H() ) - (int)( 0.5 * game->ScreenHeight() ) <= 1438 ) )
    {
        *offsetY = player->Y() - (int)( 0.5 * player->H() ) - (int)( 0.5 * game->ScreenHeight() );
    }
}

bool IsCollision( Character *character, Level *level, int dir )
{
    Rect player, tile[8];
    bool solid[8];
    player.x = character->RX();
    player.y = character->RY();
    player.w = character->RW();
    player.h = character->RH();

    if ( dir == UP )
    {
        player.y -= character->Speed();
    }
    else if ( dir == DOWN )
    {
        player.y += character->Speed();
    }
    else if ( dir == LEFT )
    {
        player.x -= character->Speed();
    }
    else if ( dir == RIGHT )
    {
        player.y += character->Speed();
    }

    //Check four surrounding tiles, on bottom and middle layers (top layer is ABOVE player, no solid ones)
    //layer 0, top-left tile
    //Set(x, y, w, h)
    tile[0].Set( level->tile[0][ player.x / 32 ][ player.y / 32 ].x, level->tile[0][ player.x / 32 ][ player.y / 32 ].y,
                    level->tile[0][ player.x / 32 ][ player.y / 32 ].w, level->tile[0][ player.x / 32 ][ player.y / 32 ].h );
    solid[0] = level->tile[0][ player.x / 32 ][ player.y / 32 ].solid;
    //layer 0, top-right tile
    tile[1].Set( level->tile[0][ player.x / 32 + 1 ][ player.y / 32 ].x, level->tile[0][ player.x / 32 + 1 ][ player.y / 32 ].y,
                    level->tile[0][ player.x / 32 + 1 ][ player.y / 32 ].w, level->tile[0][ player.x / 32 + 1 ][ player.y / 32 ].h );
    solid[1] = level->tile[0][ player.x / 32 + 1 ][ player.y / 32 ].solid;
    //layer 0, bottom-left tile
    tile[2].Set( level->tile[0][ player.x / 32 ][ player.y / 32 + 1 ].x, level->tile[0][ player.x / 32 ][ player.y / 32 + 1 ].y,
                    level->tile[0][ player.x / 32 ][ player.y / 32 + 1 ].w, level->tile[0][ player.x / 32 ][ player.y / 32 + 1 ].h );
    solid[2] = level->tile[0][ player.x / 32 ][ player.y / 32 + 1 ].solid;
    //layer 0, bottom-right tile
    tile[3].Set( level->tile[0][ player.x / 32 + 1 ][ player.y / 32 + 1 ].x, level->tile[0][ player.x / 32 + 1 ][ player.y / 32 + 1 ].y,
                    level->tile[0][ player.x / 32 + 1 ][ player.y / 32 + 1 ].w, level->tile[0][ player.x / 32 + 1 ][ player.y / 32 + 1 ].h );
    solid[3] = level->tile[0][ player.x / 32 + 1 ][ player.y / 32 + 1 ].solid;

    //layer 1, top-left tile
    tile[4].Set( level->tile[1][ player.x / 32 ][ player.y / 32 ].x, level->tile[1][ player.x / 32 ][ player.y / 32 ].y,
                    level->tile[1][ player.x / 32 ][ player.y / 32 ].w, level->tile[1][ player.x / 32 ][ player.y / 32 ].h );
    solid[4] = level->tile[1][ player.x / 32 ][ player.y / 32 ].solid;
    //layer 1, top-right tile
    tile[5].Set( level->tile[1][ player.x / 32 + 1 ][ player.y / 32 ].x, level->tile[1][ player.x / 32 + 1 ][ player.y / 32 ].y,
                    level->tile[1][ player.x / 32 + 1 ][ player.y / 32 ].w, level->tile[1][ player.x / 32 + 1 ][ player.y / 32 ].h );
    solid[5] = level->tile[1][ player.x / 32 + 1 ][ player.y / 32 ].solid;
    //layer 1, bottom-left tile
    tile[6].Set( level->tile[1][ player.x / 32 ][ player.y / 32 + 1 ].x, level->tile[1][ player.x / 32 ][ player.y / 32 + 1 ].y,
                    level->tile[1][ player.x / 32 ][ player.y / 32 + 1 ].w, level->tile[1][ player.x / 32 ][ player.y / 32 + 1 ].h );
    solid[6] = level->tile[1][ player.x / 32 ][ player.y / 32 + 1 ].solid;
    //layer 1, bottom-right tile
    tile[7].Set( level->tile[1][ player.x / 32 + 1 ][ player.y / 32 + 1 ].x, level->tile[1][ player.x / 32 + 1 ][ player.y / 32 + 1 ].y,
                    level->tile[1][ player.x / 32 + 1 ][ player.y / 32 + 1 ].w, level->tile[1][ player.x / 32 + 1 ][ player.y / 32 + 1 ].h );
    solid[7] = level->tile[1][ player.x / 32 + 1 ][ player.y / 32 + 1 ].solid;

    for ( int i=0; i<8; i++ )
    {
        if ( solid[i] == true && IsCollision( player, tile[i] ) )
            return true;
    }

    return false;
}

bool IsCollision( Character *player, Item *item )
{
    Rect pl, it;
    pl.x = player->RX();
    pl.y = player->RY();
    pl.w = player->RW();
    pl.h = player->RH();

    it.x = item->RX();
    it.y = item->RY();
    it.w = item->RW();
    it.h= item->RH();

    return IsCollision( pl, it );
}

bool IsCollision( Character *player, Enemy *enemy )
{
    Rect pl, en;
    pl.x = player->RX();
    pl.y = player->RY();
    pl.w = player->RW();
    pl.h = player->RH();

    en.x = enemy->RX();
    en.y = enemy->RY();
    en.w = enemy->RW();
    en.h= enemy->RH();

    return IsCollision( pl, en );
}

void IncrementCounter()
{
    counter++;
}

bool IsCollision( Rect r1, Rect r2 )
{
    if (    r1.x            <       r2.x+r2.w &&
            r1.x+r1.w    >       r2.x &&
            r1.y            <       r2.y+r2.h &&
            r1.y+r1.h     >       r2.y )
    {
        return true;
    }
    return false;
}

float Distance( int x1, int y1, int x2, int y2 )
{
    //a^2 + b^2 = c^2
    return ( sqrt ( (x2 - x1)*(x2 - x1) + (y2 - y1)*(y2 - y1) ) );
}

void DrawMenuText( BITMAP *buffer )
{
    int x = 520, y = 220;

                //shadow
                textprintf_centre_ex( buffer, font, x-1, y+1, makecol( 0, 0, 0 ), -1, "Press ENTER to start" );
                textprintf_centre_ex( buffer, font, x, y+1, makecol( 0, 0, 0 ), -1, "Press ENTER to start" );
                textprintf_centre_ex( buffer, font, x+1, y+1, makecol( 0, 0, 0 ), -1, "Press ENTER to start" );

                textprintf_centre_ex( buffer, font, x-1, y, makecol( 0, 0, 0 ), -1, "Press ENTER to start" );
                textprintf_centre_ex( buffer, font, x+1, y, makecol( 0, 0, 0 ), -1, "Press ENTER to start" );

                textprintf_centre_ex( buffer, font, x-1, y-1, makecol( 0, 0, 0 ), -1, "Press ENTER to start" );
                textprintf_centre_ex( buffer, font, x, y-1, makecol( 0, 0, 0 ), -1, "Press ENTER to start" );
                textprintf_centre_ex( buffer, font, x+1, y-1, makecol( 0, 0, 0 ), -1, "Press ENTER to start" );

                //gold text
                textprintf_centre_ex( buffer, font, x, y, makecol( 255, 200, 0 ), -1, "Press ENTER to start" );

                y = 230;

                //shadow
                textprintf_centre_ex( buffer, font, x-1, y+1, makecol( 0, 0, 0 ), -1, "or F4 to quit" );
                textprintf_centre_ex( buffer, font, x, y+1, makecol( 0, 0, 0 ), -1, "or F4 to quit" );
                textprintf_centre_ex( buffer, font, x+1, y+1, makecol( 0, 0, 0 ), -1, "or F4 to quit" );

                textprintf_centre_ex( buffer, font, x-1, y, makecol( 0, 0, 0 ), -1, "or F4 to quit" );
                textprintf_centre_ex( buffer, font, x+1, y, makecol( 0, 0, 0 ), -1, "or F4 to quit" );

                textprintf_centre_ex( buffer, font, x-1, y-1, makecol( 0, 0, 0 ), -1, "or F4 to quit" );
                textprintf_centre_ex( buffer, font, x, y-1, makecol( 0, 0, 0 ), -1, "or F4 to quit" );
                textprintf_centre_ex( buffer, font, x+1, y-1, makecol( 0, 0, 0 ), -1, "or F4 to quit" );

                //gold text
                textprintf_centre_ex( buffer, font, x, y, makecol( 255, 200, 0 ), -1, "or F4 to quit" );
}

void DrawMiniMap( BITMAP *buffer, Level *level, Character *player )
{
    int x = 8;
    int y = 412;
    for ( int i=0; i<TILEX; i++ )
    {
        for ( int j=0; j<TILEY; j++ )
        {
            if ( level->tile[0][i][j].solid )
                putpixel( buffer, i+x, j+y, makecol( 255, 255, 255 ) );
            if ( level->tile[1][i][j].solid )
                putpixel( buffer, i+x, j+y, makecol( 255, 255, 0 ) );

            putpixel( buffer, (player->X()/32) + x, (player->Y()/32) + y-1, makecol( 255, 0, 0 ) );//up
            putpixel( buffer, (player->X()/32) + x, (player->Y()/32) + y+1, makecol( 255, 0, 0 ) );//down
            putpixel( buffer, (player->X()/32) + x-1, (player->Y()/32) + y, makecol( 255, 0, 0 ) );//left
            putpixel( buffer, (player->X()/32) + x+1, (player->Y()/32) + y, makecol( 255, 0, 0 ) );//right
            putpixel( buffer, (player->X()/32) + x, (player->Y()/32) + y, makecol( 255, 0, 0 ) );//center

        }
    }
}




