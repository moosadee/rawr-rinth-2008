#pragma once

class Rect
{
    private:

    public:
        int x, y, w, h;
        Rect() { x = y = w = h = 0; }
        void Set( int tx, int ty, int tw, int th ) { x = tx; y = ty; w = tw; h = th; }
        int X() { return x; }
        void X( int val ) { x = val; }
        int Y() { return y; }
        void Y( int val ) { y = val; }
        int W() { return w; }
        void W( int val ) { w = val; }
        int H() { return h; }
        void H( int val ) { h = val; }
        int X1() { return x; }
        void X1( int val ) { x = val; }
        int X2() { return x+w; }
        void X2( int val ) { w = val - x; }
        int Y1() { return y; }
        void Y1( int val ) { y = val; }
        int Y2() { return y+h; }
        void Y2( int val ) { h = val - y; }
};

