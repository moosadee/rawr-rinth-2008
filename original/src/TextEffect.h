#pragma once
#include <allegro.h>
#include <string>
using namespace std;

const float increment = 1;
const float CounterAmt = 60;

class TextEffect
{
    public:
        string text;
        int x, y;
        float timer;
        int r, g, b;
        bool inUse;
        void Draw( BITMAP *buffer, int offsetX, int offsetY )
        {
            //shadow
            textprintf_centre_ex( buffer, font, x - offsetX + 6+1, y + timer - offsetY - 64, makecol( 0, 0, 0 ), -1, text.c_str() );
            textprintf_centre_ex( buffer, font, x - offsetX + 6-1, y + timer - offsetY - 64, makecol( 0, 0, 0 ), -1, text.c_str() );
            textprintf_centre_ex( buffer, font, x - offsetX + 6, y + timer - offsetY - 64+1, makecol( 0, 0, 0 ), -1, text.c_str() );
            textprintf_centre_ex( buffer, font, x - offsetX + 6, y + timer - offsetY - 64-1, makecol( 0, 0, 0 ), -1, text.c_str() );
            //text
            textprintf_centre_ex( buffer, font, x - offsetX + 6, y + timer - offsetY - 64, makecol( r, g, b ), -1, text.c_str() );
        }
        void Setup( string message, int beginX, int beginY )
        {
            Setup( message, beginX, beginY, 255, 255, 255 );
        }
        void Setup( string message, int beginX, int beginY, int beginR, int beginG, int beginB )
        {
            x = beginX;
            y = beginY;
            r = beginR;
            g = beginG;
            b = beginB;
            timer = CounterAmt;
            inUse = true;
            text = message;
        }
        TextEffect()
        {
            inUse = false;
            x = y = r = g = b = 0;
            timer = 0;
        }
        void Update()
        {
            if ( inUse )
            {
                timer -= increment;
                if ( timer <= 0 )
                {
                    inUse = false;
                }
            }
        }
};
