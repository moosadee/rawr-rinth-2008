#include "Character.h"

Character::Character()
{
    level = 1;
    x = 8*32;
    y = 36*32;
    w = 32;
    h = 32;
    frame = IDLE;
    speed = 2.0f;
    direction = RIGHT;
    action = WALKING;
    FRAMEMAX = 4;
    region.X( x + 5 );
    region.Y( y + 5 );
    region.W( 24 );
    region.H( 24 );
    hp = 100;
    score = 0;
    atkCounter = -1;
    exists = true;
    fStrength = -5;
    szStrength = "-5";
    stamina = 100;
    levelupExp = 40;
    powerup = 0;    //no power ups to begin with
}

void Character::Draw( BITMAP *destination, BITMAP *source, int offsetX, int offsetY )
{
    if ( exists )
    {
        if ( action != ATTACKING )
            masked_blit( source, destination, (int)frame * w, (int)direction * h, x - offsetX, y - offsetY, w, h );
        else
        {
            int atk;
            if ( atkCounter > 5 ) { atk = 0; }
            else { atk = 1; }
            if ( direction == LEFT )
                masked_blit( source, destination, atk*w+(w*2), 2*h, x - offsetX, y - offsetY, w, h );
            else if ( direction == RIGHT )
                masked_blit( source, destination, atk*w, 2*h, x - offsetX, y - offsetY, w, h );
        }
    }
}

void Character::Update()
{
    region.X( x + 5 );
    region.Y( y + 5 );
    region.W( 24 );
    region.H( 24 );
    if ( atkCounter > 0 )
    {
        atkCounter -= 0.5;
    }
    else { atkCounter = -1; action = WALKING; }
}

void Character::BeginAttack()
{
    if ( action != ATTACKING && atkCounter <= 0 )
    {
        action = ATTACKING;
        atkCounter = 10;
    }
}

void Character::Move( Direction dir )
{
    if ( action != ATTACKING )
    {
        if ( dir == LEFT )
        {
            direction = dir;
            x = (int)(x - speed);
        }
        else if ( dir == RIGHT )
        {
            direction = dir;
            x = (int)(x + speed);
        }

        else if ( dir == UP )
        {
            y = (int)(y - speed);
        }
        else if ( dir == DOWN )
        {
            y = (int)(y + speed);
        }

        action = WALKING;
    }
    IncrementFrame();
}

void Enemy::Update()
{
    region.X( x + 5 );
    region.Y( y + 5 );
    region.W( 24 );
    region.H( 24 );
    IncrementFrame();
}

void Enemy::Setup( int i )
{
    int tile = 32;
    exists = true;
    if ( i % 4 == 0 )
    {
        type = EWOK;
        speed = 1.2f;
        w = 32;
        h = 48;
        hp = 20;
        expAmt = 6;
    }
    else if ( i % 4 == 1 )
    {
        type = ROBOT;
        speed = 2.0f;
        w = h = 32;
        hp = 50;
        expAmt = 10;
    }
    else if ( i % 4 == 2 )
    {
        type = KITTY;
        speed = 1.5f;
        w = h = 32;
        hp = 60;
        expAmt = 12;
    }
    else
    {
        type = SNAKE;
        speed = 1.0f;
        w = 48;
        h = 32;
        hp = 100;
        expAmt = 8;
    }
    switch ( i )
    {
        case 0:
            x = 29*tile;
            y = 6*tile;
        break;
        case 1:
            x = 36*tile;
            y = 21*tile;
        break;
        case 2:
            x = 60*tile;
            y = 34*tile;
        break;
        case 3:
            x = 72*tile;
            y = 42*tile;
        break;
        case 4:
            x = 15*tile;
            y = 17*tile;
        break;
        case 5:
            x = 7*tile;
            y = 19*tile;
        break;
        case 6:
            x = 18*tile;
            y = 29*tile;
        break;
        case 7:
            x = 3*tile;
            y = 56*tile;
        break;
        case 8:
            x = 28*tile;
            y = 54*tile;
        break;
        case 9:
            x = 77*tile;
            y = 3*tile;
        break;
        case 10:
            x = 62*tile;
            y = 5*tile;
        break;
        case 11:
            x = 73*tile;
            y = 13*tile;
        break;
        case 12:
            x = 78*tile;
            y = 26*tile;
        break;
        case 13:
            x = 73*tile;
            y = 54*tile;
        break;
        case 14:
            x = 33*tile;
            y = 49*tile;
        break;
        case 15:
            x = 22*tile;
            y = 41*tile;
        break;
        case 16:
            x = 24*tile;
            y = 28*tile;
        break;
        case 17:
            x = 47*tile;
            y = 29*tile;
        break;
        case 18:
            x = 42*tile;
            y = 26*tile;
        break;
        case 19:
            x = 57*tile;
            y = 41*tile;
        break;

    }
}



