#pragma once
#include "allegro.h"
#include "Rect.h"

class Tile
{
    public:
        int x, y, w, h, fx, fy;
        Rect region;
        bool solid;
        Tile()
        {
            x = y = fx = fy = 0;
            w = h = 32;
            solid = false;
            region.X( 0 );
            region.Y( 0 );
            region.W( 32 );
            region.H( 32 );
        }
};
