#pragma once
#include "allegro.h"
#include "Rect.h"
#include "BasicObject.h"
#include <iostream>
using namespace std;

enum Direction { LEFT = 1, RIGHT = 0, UP = 2, DOWN = 3 };
enum Action { WALKING = 0, ATTACKING = 1 };
enum Type { ROBOT = 0, EWOK = 1, KITTY = 2, SNAKE = 3 };

const float WALK = 2.0;
const float RUN = 4.0;

#define IDLE 1;

class Character : public BasicObject
{
    protected:
        Direction direction;
        Action action;
        float speed;
        int score;
        float atkCounter;
        float fStrength;
        string szStrength;
        int exp, level, levelupExp;
        int powerup;
        int monies, eggs;
        float stamina;
    public:
        Character();
        void Move( Direction dir );
        float Speed() { return speed; }
        void Speed( float val ) { speed = val; }
        void Update();
        void Draw( BITMAP *destination, BITMAP *source, int offsetX, int offsetY );
        void AddScore( int amt ) { score += amt; }
        Action Is() { return action; }
        int Score() { return score; }
        void BeginAttack();
        float StrengthFloat() { return fStrength; }
        string StrengthString() { return szStrength; }
        int Level() { return level; }
        int Powerup() { return powerup; }
        void Powerup( int val ) { powerup = val; }
        int Monies() { return monies; }
        void AddMonies( int amt ) { monies += amt; }
        int Eggs() { return eggs; }
        int Stamina() { return stamina; }
        void DrainStamina() { stamina -= 1; if ( stamina < 0 ) { stamina = 0; } }
        void RestoreStamina() { stamina += 0.5; if ( stamina > 100 ) { stamina = 100; } }
        void AddEggs() { eggs += 1; }
        bool Exp( int val )
        {
            exp += val;
            if ( exp >= levelupExp )
            {
                level++;
                levelupExp += level * 1.5;
                exp = 0;
                hp += 50;
                if ( hp > 100 ) { hp = 100; }
                if ( level == 1 ) { fStrength = -5; szStrength = "-5"; }
                else if ( level == 2 ) { fStrength = -12; szStrength = "-12"; }
                else if ( level == 3 ) { fStrength = -20; szStrength = "-20"; }
                else if ( level == 4 ) { fStrength = -30; szStrength = "-30"; }
                else if ( level == 5 ) { fStrength = -50; szStrength = "-50"; }
                else if ( level == 6 ) { fStrength = -60; szStrength = "-60"; }
                return true;
            }
            return false;
        }
        int Exp() { return exp; }
        int ExpTilLevel() { return levelupExp; }
};

class Enemy : public Character
{
    public:
        int expAmt;
        Type type;
        void Setup( int i );
        void Update();
        int GivesExp() { return expAmt; }
};
