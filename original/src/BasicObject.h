#pragma once

class BasicObject
{
    protected:
        int x, y, w, h, FRAMEMAX;
        Rect region;	//collision region
        float frame;
        bool exists;
        int hp;
    public:
        void Draw( BITMAP *destination, BITMAP *source, int offsetX, int offsetY )
        {
            if ( exists )
                masked_blit( source, destination, (int)frame * w, 0, x - offsetX, y - offsetY, w, h );
        }

        void Update();
        void IncrementFrame()
        {
            frame += 0.15f;
            if ( frame >= FRAMEMAX )
                frame = 0.0f;
        }
        int X() { return x; }
        int Y() { return y; }
        int W() { return w; }
        int H() { return h; }
        int RX() { return region.x; }
        int RY() { return region.y; }
        int RW() { return region.w; }
        int RH() { return region.h; }
        bool Exists() { return exists; }
        void Exists( bool val ) { exists = val; }
        Rect CollisionRegion() { return region; }
        int HP() { return hp; }
        void AddHP( int amt )
        {
            hp += amt;
            if ( hp > 100 )
            {
                hp = 100;
            }
        }
};

