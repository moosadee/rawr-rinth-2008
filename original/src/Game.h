#pragma once
#include "allegro.h"
#include <string>
#include <iostream>
using namespace std;

enum GameState { sMENU = 0, sINGAME = 1, sGAMEOVER = 2 };

class Game
{
	private:
		int screenWidth, screenHeight;
		bool done, fullscreen;
		GameState state;
	public:
		Game();
		~Game();

		int ScreenWidth();
		int ScreenHeight();
		int FPS();

		bool Done() { return done; }
		void Done( bool val ) { done = val; }

		void ToggleFullscreen();

		void BeginDraw();
		void EndDraw( BITMAP *buffer );

		GameState State() { return state; }
		void State( GameState val ) { state = val; }
};
