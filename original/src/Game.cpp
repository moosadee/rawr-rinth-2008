#include "Game.h"

Game::Game()
{
    done = false;
    fullscreen = true;
	screenWidth = 640;
	screenHeight = 480;
	text_mode(-1);
	state = sMENU;

	allegro_init();
    install_keyboard();
    install_timer();
    install_mouse();
    install_sound(DIGI_AUTODETECT, MIDI_AUTODETECT, 0);
    install_joystick(JOY_TYPE_AUTODETECT);
    set_color_depth(16);
    if ( fullscreen ) { set_gfx_mode(GFX_AUTODETECT, screenWidth, screenHeight, 0, 0); }
    else { set_gfx_mode(GFX_AUTODETECT_WINDOWED, screenWidth, screenHeight, 0, 0); }
}
Game::~Game()
{
}

int Game::ScreenWidth() { return screenWidth; }
int Game::ScreenHeight() { return screenHeight; }

void Game::ToggleFullscreen()
{
    if ( fullscreen )
    {
        fullscreen = false;
        set_gfx_mode(GFX_AUTODETECT_WINDOWED, screenWidth, screenHeight, 0, 0);
    }
    else
    {
        fullscreen = true;
        set_gfx_mode(GFX_AUTODETECT, screenWidth, screenHeight, 0, 0);
    }
}
void Game::EndDraw( BITMAP *buffer )
{
	blit( buffer, screen, 0, 0, 0, 0, screenWidth, screenHeight );
	release_screen();
	clear_bitmap( buffer );
}

void Game::BeginDraw()
{
    vsync();
    acquire_screen();
}
